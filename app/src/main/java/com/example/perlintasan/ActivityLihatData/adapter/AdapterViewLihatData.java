package com.example.perlintasan.ActivityLihatData.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.perlintasan.ActivityLihatData.ActivityLihatData;
import com.example.perlintasan.ActivityLihatData.model.Model;
import com.example.perlintasan.ActivityTambahData.ActivityUpdateData;
import com.example.perlintasan.DatabaseHelper.DatabaseHandler;
import com.example.perlintasan.R;
import com.example.perlintasan.helper.BaseActivity;
import com.example.perlintasan.helper.SessionManager;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterViewLihatData extends BaseAdapter {

    private Context context;
    private int layout;
    private ArrayList<Model> ListData;
    private String ket;

    public static DatabaseHandler databaseHandler;

    public AdapterViewLihatData(Context context, int item_data, ArrayList<Model> mList, String ket) {
        this.context = context;
        this.layout = item_data;
        this.ListData = mList;
        this.ket = ket;

        databaseHandler = new DatabaseHandler(context);
    }

    @Override
    public int getCount() {
        return ListData.size();
    }

    @Override
    public Object getItem(int position) {
        return ListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class viewHolder{
        TextView AdTxtNo, AdTxtId, AdTxtNoPaspor, AdtxtNama, AdtxtanggalLahir;
        ImageView AdTxtImage;
        LinearLayout linear;
        CardView cv;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup ) {
        View row = view;
        viewHolder holder = new viewHolder();

        if (row == null)
        {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);

            holder.AdTxtNo = row.findViewById(R.id.tvNo);
            holder.AdTxtId = row.findViewById(R.id.tvId);
            holder.AdTxtImage = row.findViewById(R.id.itemImage);
            holder.AdTxtNoPaspor = row.findViewById(R.id.tvNoPaspor);
            holder.AdtxtNama = row.findViewById(R.id.tvNama);
            holder.AdtxtanggalLahir = row.findViewById(R.id.tvTanggalLahir);
            holder.linear = row.findViewById(R.id.linear);

            holder.linear.setTag(ListData.get(i).getId());
            row.setTag(holder);

            holder.linear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                    CharSequence[] items = {"Ubah", "Hapus"};
                    dialog.setTitle("Pilih tindakan");
                    //Toast.makeText(context,"id = "+v.getTag(),Toast.LENGTH_SHORT).show();
                    dialog.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            if (i == 0)
                            {
                                int id = (int) v.getTag();
                                Intent myIntent = new Intent(context, ActivityUpdateData.class);
                                myIntent.putExtra("id", id);
                                myIntent.putExtra("keterangan", ket);
                                context.startActivity(myIntent);
                            }
                            if (i == 1)
                            {
                                int id = (int)v.getTag();
                                showDialogDelete(id);
                            }
                        }
                    });
                    dialog.show();
                }
            });
        }
        else
        {
            holder = (viewHolder)row.getTag();
        }
        Model model = ListData.get(i);

        holder.AdTxtNo.setText(String.valueOf(model.getNo()));
        holder.AdTxtId.setText(String.valueOf(model.getId()));

        String imageData = model.getImage();
        byte[] decodedByte = Base64.decode(imageData, 0);
        holder.AdTxtImage.setImageBitmap(BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length));

        holder.AdTxtNoPaspor.setText(String.valueOf(model.getNomor_pspor()));
        holder.AdtxtNama.setText(model.getNama());
        holder.AdtxtanggalLahir.setText(model.getTanggalLahir());

        return row;
    }

    private void showDialogDelete(final int idRecord) {
        AlertDialog.Builder dialogDelete = new AlertDialog.Builder(context);
        dialogDelete.setTitle("Peringatan !");
        dialogDelete.setMessage("Yakin hapus data ?");
        dialogDelete.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                try
                {
                    databaseHandler.delete(idRecord);
                    Toast.makeText(context,"Hapus sukses !", Toast.LENGTH_SHORT).show();
                    Intent thisIntent = new Intent(context, ActivityLihatData.class);
                    thisIntent.putExtra("keterangan", ket);
                    context.startActivity(thisIntent);
                }
                catch (Exception ex)
                {
                    Log.e("error : ", ex.getMessage());
                }
            }
        });
        dialogDelete.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                dialogInterface.dismiss();
            }
        });
        dialogDelete.show();
    }
}
