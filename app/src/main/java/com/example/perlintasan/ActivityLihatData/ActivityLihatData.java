package com.example.perlintasan.ActivityLihatData;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.perlintasan.ActivityLihatData.adapter.AdapterViewLihatData;
import com.example.perlintasan.ActivityLihatData.model.Model;
import com.example.perlintasan.DatabaseHelper.DatabaseHandler;
import com.example.perlintasan.R;
import com.example.perlintasan.helper.AppsHelper;
import com.example.perlintasan.helper.BaseActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityLihatData extends BaseActivity {

    @BindView(R.id.listData)
    ListView listData;
    @BindView(R.id.tvKeterangan)
    TextView tvKeterangan;

    public static DatabaseHandler databaseHandler;

    ArrayList<Model> mList;
    AdapterViewLihatData mAdapter = null;

    String keterangan;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_data);
        ButterKnife.bind(this);

        databaseHandler = new DatabaseHandler(this);

        tvKeterangan.setText(getIntent().getStringExtra("keterangan"));
        keterangan = tvKeterangan.getText().toString();
        if(keterangan.equals(""))
        {
            tvKeterangan.setText(sesi.getKeterangan());
        }

        mList = new ArrayList<>();
        mAdapter = new AdapterViewLihatData(this, R.layout.item_data, mList, tvKeterangan.getText().toString());

        listData.setAdapter(mAdapter);

        Cursor cursor = databaseHandler.getDataByNipAndKeterangan(sesi.getNip(), tvKeterangan.getText().toString());

        mList.clear();
        int no = 1;
        while (cursor.moveToNext()) {
            int urut = no;
            int id = cursor.getInt(0);
            String no_paspor = cursor.getString(1);
            String nama = cursor.getString(2);
            String tanggalLahir = cursor.getString(3);
            String image = cursor.getString(13);

            mList.add(new Model(urut, id, image, no_paspor, nama, tanggalLahir));
            no++;
        }

        mAdapter.notifyDataSetChanged();
        if (mList.size() == 0) {
            AppsHelper.ShowMessageInfo( "No Data Found...", this);
        }
    }
}
