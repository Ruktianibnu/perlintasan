package com.example.perlintasan.ActivityLihatData.model;

public class JamaahList {
    int ID;
    String NO_PASPOR;
    String NAMA;
    String TANGGAL_LAHIR;
    String TEMPAT_LAHIR;
    String WARGA_NEGARA;
    String JENIS_KELAMIN;
    String TANGGAL_PENGELUARAN;
    String TANGGAL_BERLAKU;
    String TANGGAL_MELINTAS;
    String KODE_TPI;
    String KODE_PENERBANGAN;
    String NIP;
    String IMAGE;
    String KETERANGAN;
    String CREATED_AT;

    public JamaahList() {
        this.ID = ID;
        this.NO_PASPOR = NO_PASPOR;
        this.NAMA = NAMA;
        this.TANGGAL_LAHIR = TANGGAL_LAHIR;
        this.TEMPAT_LAHIR = TEMPAT_LAHIR;
        this.WARGA_NEGARA = WARGA_NEGARA;
        this.JENIS_KELAMIN = JENIS_KELAMIN;
        this.TANGGAL_PENGELUARAN = TANGGAL_PENGELUARAN;
        this.TANGGAL_BERLAKU = TANGGAL_BERLAKU;
        this.TANGGAL_MELINTAS = TANGGAL_MELINTAS;
        this.KODE_TPI = KODE_TPI;
        this.KODE_PENERBANGAN = KODE_PENERBANGAN;
        this.NIP = NIP;
        this.IMAGE = IMAGE;
        this.KETERANGAN = KETERANGAN;
        this.CREATED_AT = CREATED_AT;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNO_PASPOR() {
        return NO_PASPOR;
    }

    public void setNO_PASPOR(String NO_PASPOR) {
        this.NO_PASPOR = NO_PASPOR;
    }

    public String getNAMA() {
        return NAMA;
    }

    public void setNAMA(String NAMA) {
        this.NAMA = NAMA;
    }

    public String getTANGGAL_LAHIR() {
        return TANGGAL_LAHIR;
    }

    public void setTANGGAL_LAHIR(String TANGGAL_LAHIR) {
        this.TANGGAL_LAHIR = TANGGAL_LAHIR;
    }

    public String getTEMPAT_LAHIR() {
        return TEMPAT_LAHIR;
    }

    public void setTEMPAT_LAHIR(String TEMPAT_LAHIR) {
        this.TEMPAT_LAHIR = TEMPAT_LAHIR;
    }

    public String getWARGA_NEGARA() {
        return WARGA_NEGARA;
    }

    public void setWARGA_NEGARA(String WARGA_NEGARA) {
        this.WARGA_NEGARA = WARGA_NEGARA;
    }

    public String getJENIS_KELAMIN() {
        return JENIS_KELAMIN;
    }

    public void setJENIS_KELAMIN(String JENIS_KELAMIN) {
        this.JENIS_KELAMIN = JENIS_KELAMIN;
    }

    public String getTANGGAL_PENGELUARAN() {
        return TANGGAL_PENGELUARAN;
    }

    public void setTANGGAL_PENGELUARAN(String TANGGAL_PENGELUARAN) {
        this.TANGGAL_PENGELUARAN = TANGGAL_PENGELUARAN;
    }

    public String getTANGGAL_BERLAKU() {
        return TANGGAL_BERLAKU;
    }

    public void setTANGGAL_BERLAKU(String TANGGAL_BERLAKU) {
        this.TANGGAL_BERLAKU = TANGGAL_BERLAKU;
    }

    public String getTANGGAL_MELINTAS() {
        return TANGGAL_MELINTAS;
    }

    public void setTANGGAL_MELINTAS(String TANGGAL_MELINTAS) {
        this.TANGGAL_MELINTAS = TANGGAL_MELINTAS;
    }

    public String getKODE_TPI() {
        return KODE_TPI;
    }

    public void setKODE_TPI(String KODE_TPI) {
        this.KODE_TPI = KODE_TPI;
    }

    public String getKODE_PENERBANGAN() {
        return KODE_PENERBANGAN;
    }

    public void setKODE_PENERBANGAN(String KODE_PENERBANGAN) {
        this.KODE_PENERBANGAN = KODE_PENERBANGAN;
    }

    public String getNIP() {
        return NIP;
    }

    public void setNIP(String NIP) {
        this.NIP = NIP;
    }

    public String getIMAGE() {
        return IMAGE;
    }

    public void setIMAGE(String IMAGE) {
        this.IMAGE = IMAGE;
    }

    public String getKETERANGAN() {
        return KETERANGAN;
    }

    public void setKETERANGAN(String KETERANGAN) {
        this.KETERANGAN = KETERANGAN;
    }

    public String getCREATED_AT() {
        return CREATED_AT;
    }

    public void setCREATED_AT(String CREATED_AT) {
        this.CREATED_AT = CREATED_AT;
    }
}
