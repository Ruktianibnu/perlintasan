package com.example.perlintasan.network;

import com.example.perlintasan.ActivityLogin.model.ResponseLogin;
import com.example.perlintasan.ActivityLogin.model_penerbangan.ResponsePenerbangan;
import com.example.perlintasan.ActivityMenu.model.ResponseInsertMysql;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApiService {
    @FormUrlEncoded
    @POST("login")
    Call<ResponseLogin> loginUser(
            @Field("nip") String Nip,
            @Field("password") String Password
    );

    @FormUrlEncoded
    @POST("savejamaah")
    Call<ResponseInsertMysql> Sync(
            @Header("Authorization")String authToken,
            @Header("Content-Type")String ct,
            @Header("Accept")String a,
            @Field("no_paspor") String no_paspor,
            @Field("nama") String nama,
            @Field("tanggal_lahir") String tanggal_lahir,
            @Field("tempat_lahir") String tempat_lahir,
            @Field("warga_negara") String warga_negara,
            @Field("jenis_kelamin") String jenis_kelamin,
            @Field("tanggal_pengeluaran") String tanggal_pengeluaran,
            @Field("tanggal_berlaku") String tanggal_berlaku,
            @Field("tanggal_melintas") String tanggal_melintas,
            @Field("kode_tpi") String kode_tpi,
            @Field("kode_penerbangan") String kode_penerbangan,
            @Field("nip") String nip,
            @Field("image") String image,
            @Field("keterangan") String keterangan,
            @Field("created_at") String created_at
    );

    @FormUrlEncoded
    @POST("getDataPenerbangan")
    Call<ResponsePenerbangan> getDataPenerbangan(
            @Header("Authorization")String authToken,
            @Header("Content-Type")String ct,
            @Header("Accept")String a,
            @Field("kode_tpi") String kode_tpi
    );
}
