package com.example.perlintasan;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.perlintasan.ActivityLogin.ActivityLogin;
import com.example.perlintasan.ActivityMenu.MenuActivity;
import com.example.perlintasan.helper.BaseActivity;

public class MainActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(sesi.isLogin()) {
                    startActivity(new Intent(MainActivity.this, MenuActivity.class));
                } else {
                    startActivity(new Intent(MainActivity.this, ActivityLogin.class));
                }
                finish();
            }
        }, 500);
    }
}
