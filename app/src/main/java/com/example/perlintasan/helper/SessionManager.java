package com.example.perlintasan.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.perlintasan.ActivityLogin.model_penerbangan.DatasItem;
import com.example.perlintasan.ActivityLogin.model_penerbangan.ResponsePenerbangan;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Callback;

public class SessionManager {
    private static final String KEY_TOKEN = "tokenLogin";
    private static final String KEY_LOGIN = "isLogin";

    //untuk menyimpan session menggunakan shared preferences
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    int PRIVATE_MODE = 0;

    Context context;

    String PREF_NAME = "LoginAppsPerlintasan";
    private Object ModelDataPenerbangan;

    public SessionManager(Context c) {
        this.context = c;
        pref = c.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    //membuat session login
    public void createLoginSession(String token) {
        editor.putString(KEY_TOKEN, token);
        editor.putBoolean(KEY_LOGIN, true);
        editor.commit();
    }

    //mendapatkan TOKEN
    public String getToken() {
        return pref.getString(KEY_TOKEN, "");
    }

    public void setToken(String token){
        editor.putString("token", token);
        editor.commit();
    }

    //cek login
    public boolean isLogin() {
        return pref.getBoolean(KEY_LOGIN, false);
    }

    //logout user
    public void logoutUser() {
        editor.clear();
        editor.commit();
    }

    public void setNama(String nama){
        editor.putString("nama", nama);
        editor.commit();
    }

    public String getNama() {
        return pref.getString("nama", "");
    }

    public void setKode_Kantor(String kode_kantor){
        editor.putString("kode_kantor", kode_kantor);
        editor.commit();
    }

    public String getKode_Kantor() {
        return pref.getString("kode_kantor", "");
    }

    public void setNip(String nip){
        editor.putString("nip", nip);
        editor.commit();
    }

    public String getNip() {
        return pref.getString("nip", "");
    }

    public void setNama_Kantor(String nama_kantor) {
        editor.putString("nama_kantor", nama_kantor);
        editor.commit();
    }

    public String getNama_Kantor() {
        return pref.getString("nama_kantor", "");
    }

    public void setKeterangan(String keterangan) {
        editor.putString("keterangan", keterangan);
        editor.commit();
    }

    public String getKeterangan() {
        return pref.getString("keterangan", "");
    }
}
