package com.example.perlintasan.ActivityHistory.model;

import android.content.Context;

import com.example.perlintasan.ActivityLihatData.model.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Model_keberangkatan {
    private int no;
    private int id;
    private String nomor_pspor;
    private String nama;
    private String tanggalLahir;

    public Model_keberangkatan(int no, int id, String nomor_pspor, String nama, String tanggalLahir) {
        this.no = no;
        this.id = id;
        this.nomor_pspor = nomor_pspor;
        this.nama = nama;
        this.tanggalLahir = tanggalLahir;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomor_pspor() {
        return nomor_pspor;
    }

    public void setNomor_pspor(String nomor_pspor) {
        this.nomor_pspor = nomor_pspor;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }
}
