package com.example.perlintasan.ActivityHistory;

import android.database.Cursor;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.perlintasan.ActivityHistory.adapter.AdapterKeberangkatan;
import com.example.perlintasan.ActivityHistory.adapter.AdapterKedatangan;
import com.example.perlintasan.ActivityHistory.model.Model_Kedatangan;
import com.example.perlintasan.ActivityHistory.model.Model_keberangkatan;
import com.example.perlintasan.ActivityLihatData.adapter.AdapterViewLihatData;
import com.example.perlintasan.ActivityLihatData.model.Model;
import com.example.perlintasan.DatabaseHelper.DatabaseHandler;
import com.example.perlintasan.R;
import com.example.perlintasan.helper.AppsHelper;
import com.example.perlintasan.helper.BaseActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityHistory extends BaseActivity {

    @BindView(R.id.etNama)
    EditText etNama;
    @BindView(R.id.btnSearch)
    ImageButton btnSearch;
    @BindView(R.id.linearLayout)
    LinearLayout linearLayout;
    @BindView(R.id.lvHistory)
    ListView lvHistory;

    public static DatabaseHandler databaseHandler;

    ArrayList<Model_keberangkatan> mListKeberangkatan;
    ArrayList<Model_Kedatangan> mListKedatangan;
    AdapterKeberangkatan mAdapterKeberangkatan = null;
    AdapterKedatangan mAdapterKedatangan = null;
    String keterangan;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);

        databaseHandler = new DatabaseHandler(this);

        keterangan = getIntent().getStringExtra("keterangan");

        mListKeberangkatan = new ArrayList<>();
        mListKedatangan = new ArrayList<>();

         if(keterangan.equals("Keberangkatan"))
        {
            viewKeberangkatan(keterangan);
        }
        else
        {
            viewKedatangan(keterangan);
        }
    }

    @OnClick(R.id.btnSearch)
    public void onViewClicked() {
        String getnama = etNama.getText().toString();
        if(keterangan.equals("Keberangkatan"))
        {
            viewKeberangkatanByNama(keterangan, getnama);
        }
        else if(keterangan.equals("Kedatangan"))
        {
            viewKedatanganByNama(keterangan, getnama);
        }

    }

    private void viewKeberangkatan(String keterangan)
    {
        mAdapterKeberangkatan = new AdapterKeberangkatan(this,R.layout.item_history_keberangkatan, mListKeberangkatan, keterangan);
        lvHistory.setAdapter(mAdapterKeberangkatan);
        Cursor cursor = databaseHandler.getDataHistoryByNipAndKeterangan(sesi.getNip(), keterangan);
        mListKeberangkatan.clear();
        int no = 1;
        while (cursor.moveToNext())
        {
            int urut = no;
            int id = cursor.getInt(0);
            String no_paspor = cursor.getString(1);
            String nama = cursor.getString(2);
            String tanggalLahir = cursor.getString(3);

            mListKeberangkatan.add(new Model_keberangkatan(urut, id, no_paspor, nama, tanggalLahir));
            no++;
        }
        mAdapterKeberangkatan.notifyDataSetChanged();
        if (mListKeberangkatan.size() == 0) {
            AppsHelper.ShowMessageInfo("No Data Found...", this);
        }
    }

    private void viewKedatangan(String keterangan)
    {
        mAdapterKedatangan = new AdapterKedatangan(this,R.layout.item_history_kedatangan, mListKedatangan, keterangan);
        lvHistory.setAdapter(mAdapterKedatangan);
        Cursor cursor = databaseHandler.getDataHistoryByNipAndKeterangan(sesi.getNip(), keterangan);
        mListKedatangan.clear();
        int no = 1;
        while (cursor.moveToNext()) {
            int urut = no;
            int id = cursor.getInt(0);
            String no_paspor = cursor.getString(1);
            String nama = cursor.getString(2);
            String tanggalLahir = cursor.getString(3);

            mListKedatangan.add(new Model_Kedatangan(urut, id, no_paspor, nama, tanggalLahir));
            no++;
        }

        mAdapterKedatangan.notifyDataSetChanged();
        if (mListKedatangan.size() == 0) {
            AppsHelper.ShowMessageInfo("No Data Found...", this);
        }
    }

    private void viewKeberangkatanByNama(String keterangan, String getnama)
    {
        Cursor cursor;
        mAdapterKeberangkatan = new AdapterKeberangkatan(this,R.layout.item_history_keberangkatan, mListKeberangkatan, keterangan);
        lvHistory.setAdapter(mAdapterKeberangkatan);
        if(getnama.equals(""))
        {
            cursor = databaseHandler.getDataHistoryByNipAndKeterangan(sesi.getNip(), keterangan);
        }
        else
        {
            cursor = databaseHandler.getDataHistoryByNipNamaAndKeterangan(sesi.getNip(), keterangan, getnama);
        }

        mListKeberangkatan.clear();
        int no = 1;
        while (cursor.moveToNext()) {
            int urut = no;
            int id = cursor.getInt(0);
            String no_paspor = cursor.getString(1);
            String nama = cursor.getString(2);
            String tanggalLahir = cursor.getString(3);

            mListKeberangkatan.add(new Model_keberangkatan(urut, id, no_paspor, nama, tanggalLahir));
            no++;
        }
        mAdapterKeberangkatan.notifyDataSetChanged();
        if (mListKeberangkatan.size() == 0) {
            AppsHelper.ShowMessageInfo("No Data Found...", this);
        }
    }

    private void viewKedatanganByNama(String keterangan, String getnama)
    {
        Cursor cursor;
        mAdapterKedatangan = new AdapterKedatangan(this,R.layout.item_history_kedatangan, mListKedatangan, keterangan);
        lvHistory.setAdapter(mAdapterKedatangan);
        if(getnama.equals(""))
        {
            cursor = databaseHandler.getDataHistoryByNipAndKeterangan(sesi.getNip(), keterangan);
        }
        else
        {
            cursor = databaseHandler.getDataHistoryByNipNamaAndKeterangan(sesi.getNip(), keterangan, getnama);
        }

        mListKedatangan.clear();
        int no = 1;
        while (cursor.moveToNext()) {
            int urut = no;
            int id = cursor.getInt(0);
            String no_paspor = cursor.getString(1);
            String nama = cursor.getString(2);
            String tanggalLahir = cursor.getString(3);

            mListKedatangan.add(new Model_Kedatangan(urut, id, no_paspor, nama, tanggalLahir));
            no++;
        }
        mAdapterKedatangan.notifyDataSetChanged();
        if (mListKedatangan.size() == 0) {
            AppsHelper.ShowMessageInfo("No Data Found...", this);
        }
    }
}
