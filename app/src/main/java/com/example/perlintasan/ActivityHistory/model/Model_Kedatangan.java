package com.example.perlintasan.ActivityHistory.model;

public class Model_Kedatangan {
    private int no;
    private int id;
    private String nomor_paspor;
    private String nama;
    private String tanggalLahir;

    public Model_Kedatangan(int no, int id, String nomor_paspor, String nama, String tanggalLahir) {
        this.no = no;
        this.id = id;
        this.nomor_paspor = nomor_paspor;
        this.nama = nama;
        this.tanggalLahir = tanggalLahir;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomor_paspor() {
        return nomor_paspor;
    }

    public void setNomor_paspor(String nomor_pspor) {
        this.nomor_paspor = nomor_pspor;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }
}