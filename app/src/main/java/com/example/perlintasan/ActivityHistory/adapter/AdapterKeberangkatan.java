package com.example.perlintasan.ActivityHistory.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.perlintasan.ActivityHistory.model.Model_keberangkatan;
import com.example.perlintasan.ActivityLihatData.adapter.AdapterViewLihatData;
import com.example.perlintasan.ActivityLihatData.model.Model;
import com.example.perlintasan.ActivityTambahData.ActivityUpdateData;
import com.example.perlintasan.DatabaseHelper.DatabaseHandler;
import com.example.perlintasan.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterKeberangkatan extends BaseAdapter {
    private Context context;
    private int layout;
    private ArrayList<Model_keberangkatan> ListData;
    private String keterangan;
    public static DatabaseHandler databaseHandler;

    public AdapterKeberangkatan(Context context, int layout, ArrayList<Model_keberangkatan> listData, String keterangan) {
        this.context = context;
        this.layout = layout;
        ListData = listData;
        this.keterangan = keterangan;

        databaseHandler = new DatabaseHandler(context);
    }

    @Override
    public int getCount() {
        return ListData.size();
    }

    @Override
    public Object getItem(int position) {
        return ListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class viewHolder{
        TextView AdTxtNo, AdTxtId, AdTxtNoPaspor, AdtxtNama, AdtxtanggalLahir;
        LinearLayout linear;
        CardView cv;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View row = view;
        AdapterKeberangkatan.viewHolder holder = new AdapterKeberangkatan.viewHolder();

        if (row == null)
        {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);

            holder.AdTxtNo = row.findViewById(R.id.tvNo);
            holder.AdTxtId = row.findViewById(R.id.tvId);
            holder.AdTxtNoPaspor = row.findViewById(R.id.tvNoPaspor);
            holder.AdtxtNama = row.findViewById(R.id.tvNama);
            holder.AdtxtanggalLahir = row.findViewById(R.id.tvTanggalLahir);
            holder.linear = row.findViewById(R.id.linear);

            holder.linear.setTag(ListData.get(i).getId());
            row.setTag(holder);
        }
        else
        {
            holder = (AdapterKeberangkatan.viewHolder)row.getTag();
        }
        Model_keberangkatan model = ListData.get(i);

        holder.AdTxtNo.setText(String.valueOf(model.getNo()));
        holder.AdTxtId.setText(String.valueOf(model.getId()));

        holder.AdTxtNoPaspor.setText(String.valueOf(model.getNomor_pspor()));
        holder.AdtxtNama.setText(model.getNama());
        holder.AdtxtanggalLahir.setText(model.getTanggalLahir());

        return row;
    }
}
