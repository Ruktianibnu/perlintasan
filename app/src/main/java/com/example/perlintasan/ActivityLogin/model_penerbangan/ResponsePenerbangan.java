package com.example.perlintasan.ActivityLogin.model_penerbangan;

import java.util.ArrayList;
import java.util.List;

public class ResponsePenerbangan{
	private String pesan;
	private ArrayList<DatasItem> datas;
	private boolean status;

	public void setPesan(String pesan){
		this.pesan = pesan;
	}

	public String getPesan(){
		return pesan;
	}

	public void setDatas(ArrayList<DatasItem> datas){
		this.datas = datas;
	}

	public ArrayList<DatasItem> getDatas(){
		return datas;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ResponsePenerbangan{" + 
			"pesan = '" + pesan + '\'' + 
			",datas = '" + datas + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}