package com.example.perlintasan.ActivityLogin.model;

import com.google.gson.annotations.SerializedName;

public class ResponseLogin{
	@SerializedName("pesan")
	private String pesan;

	@SerializedName("datas")
	private DataLogin datas;

	@SerializedName("token")
	private String token;

	@SerializedName("status")
	private boolean status;;

	public void setPesan(String pesan){
		this.pesan = pesan;
	}

	public String getPesan(){
		return pesan;
	}

	public void setDataLogin(DataLogin datas){
		this.datas = datas;
	}

	public DataLogin getDataLogin(){
		return datas;
	}

	public void setToken(String token){
		this.token = token;
	}

	public String getToken(){
		return token;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ResponseLogin{" + 
			"pesan = '" + pesan + '\'' + 
			",datas = '" + datas + '\'' +
			",token = '" + token + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}
