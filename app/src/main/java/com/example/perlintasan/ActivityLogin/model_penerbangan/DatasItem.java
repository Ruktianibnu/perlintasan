package com.example.perlintasan.ActivityLogin.model_penerbangan;

import java.util.ArrayList;

public class DatasItem{
	private String nama_penerbangan;
	private String display;
	private String kode_penerbangan;

	public DatasItem(ArrayList<DatasItem> di) {

	}

	public void setNamaPenerbangan(String namaPenerbangan){
		this.nama_penerbangan = nama_penerbangan;
	}

	public String getNamaPenerbangan(){
		return nama_penerbangan;
	}

	public void setDisplay(String display){
		this.display = display;
	}

	public String getDisplay(){
		return display;
	}

	public void setKodePenerbangan(String kode_penerbangan){
		this.kode_penerbangan = kode_penerbangan;
	}

	public String getKodePenerbangan(){
		return kode_penerbangan;
	}

	@Override
 	public String toString(){
		return 
			"DatasItem{" + 
			"nama_penerbangan = '" + nama_penerbangan + '\'' +
			",display = '" + display + '\'' + 
			",kode_penerbangan = '" + kode_penerbangan + '\'' +
			"}";
		}
}
