package com.example.perlintasan.ActivityLogin.model;

import com.google.gson.annotations.SerializedName;

public class DataLogin {
	@SerializedName("nip")
	private String nip;

	@SerializedName("nama")
	private String nama;

	@SerializedName("nama_kantor")
	private String nama_kantor;

	@SerializedName("kode_kantor")
	private String kode_kantor;

	public void setNip(String nip){
		this.nip = nip;
	}

	public String getNip(){
		return nip;
	}

	public void setNama(String nama){
		this.nama = nama;
	}

	public String getNama(){
		return nama;
	}

	public void setNama_Kantor(String nama_kantor){
		this.nama_kantor = nama_kantor;
	}

	public String getNama_Kantor(){
		return nama_kantor;
	}

	public void setKode_Kantor(String kode_kantor){
		this.kode_kantor = kode_kantor;
	}

	public String getKode_Kantor(){
		return kode_kantor;
	}

	@Override
 	public String toString(){
		return 
			"datas{" +
			"nip = '" + nip + '\'' + 
			",nama = '" + nama + '\'' + 
			",nama_kantor = '" + nama_kantor + '\'' +
			",kode_kantor = '" + kode_kantor + '\'' +
			"}";
		}
}
