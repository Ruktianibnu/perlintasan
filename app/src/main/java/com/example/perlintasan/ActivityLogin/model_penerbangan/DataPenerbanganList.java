package com.example.perlintasan.ActivityLogin.model_penerbangan;

public class DataPenerbanganList {
    String KODE_PENERBANGAN;
    String NAMA_PENERBANGAN;

    public DataPenerbanganList() {
        this.KODE_PENERBANGAN = KODE_PENERBANGAN;
        this.NAMA_PENERBANGAN = NAMA_PENERBANGAN;
    }

    public String getKODE_PENERBANGAN() {
        return KODE_PENERBANGAN;
    }

    public void setKODE_PENERBANGAN(String KODE_PENERBANGAN) {
        this.KODE_PENERBANGAN = KODE_PENERBANGAN;
    }

    public String getNAMA_PENERBANGAN() {
        return NAMA_PENERBANGAN;
    }

    public void setNAMA_PENERBANGAN(String NAMA_PENERBANGAN) {
        this.NAMA_PENERBANGAN = NAMA_PENERBANGAN;
    }
}
