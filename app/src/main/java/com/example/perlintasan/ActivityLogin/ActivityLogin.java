package com.example.perlintasan.ActivityLogin;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.security.NetworkSecurityPolicy;
import android.support.annotation.RequiresApi;
import android.webkit.ConsoleMessage;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.perlintasan.ActivityLihatData.model.JamaahList;
import com.example.perlintasan.ActivityLogin.model.DataLogin;
import com.example.perlintasan.ActivityLogin.model.ResponseLogin;
import com.example.perlintasan.ActivityLogin.model_penerbangan.DataPenerbanganList;
import com.example.perlintasan.ActivityLogin.model_penerbangan.DatasItem;
import com.example.perlintasan.ActivityLogin.model_penerbangan.ModelPenerbangan;
import com.example.perlintasan.ActivityLogin.model_penerbangan.ResponsePenerbangan;
import com.example.perlintasan.ActivityMenu.MenuActivity;
import com.example.perlintasan.ActivityTambahData.ActivityTambahData;
import com.example.perlintasan.DatabaseHelper.DatabaseHandler;
import com.example.perlintasan.R;
import com.example.perlintasan.helper.AppsHelper;
import com.example.perlintasan.helper.BaseActivity;
import com.example.perlintasan.network.NetworkClient;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityLogin extends BaseActivity {


    private static final int PERMISSION_REQUEST_READ_CONTACT = 100;
    @BindView(R.id.imageView4)
    ImageView imageView4;
    @BindView(R.id.etNip)
    EditText etNip;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.button_login)
    Button buttonLogin;
    @BindView(R.id.welcomeInfo)
    LinearLayout welcomeInfo;

    String nip, password, pesan;
    Boolean result;
    ResponseLogin responseLogin;
    public static DatabaseHandler databaseHandler;
    ProgressDialog progressDialog;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        databaseHandler = new DatabaseHandler(this);
    }

    private void actionLogin() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_READ_CONTACT);
        } else {
/*            String dUUID = (String) AppsHelper.getDeviceUUID(c);*/
            String dUUID = "abc123";

            nip = etNip.getText().toString();
            password = etPassword.getText().toString();

            if(checkInternet())
            {
                if (nip.equals("") || password.equals(""))
                {
                    AppsHelper.ShowMessageError("nip dan password tidak boleh kosong", this);
                }
                else
                {
                    progressDialog = new ProgressDialog(ActivityLogin.this);
                    progressDialog.setMax(0);
                    progressDialog.setMessage("Mohon Tunggu");
                    progressDialog.setTitle("Sedang Mencoba Akses Server");
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    for(int a = 1; a < 19; a++)
                    {
                        progressDialog.incrementProgressBy(a);
                        progressDialog.show();
                    }

                    try{
                        NetworkClient.service.loginUser(nip, password).enqueue(new Callback<ResponseLogin>() {
                            @Override
                            public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                                if (response.isSuccessful()) {
                                    pesan = response.body().getPesan();
                                    result = response.body().isStatus();
                                    String token = response.body().getToken();

                                    if (result) {
                                        assert response.body() != null;
                                        DataLogin dataLogin = response.body().getDataLogin();
                                        sesi.setToken(token);
                                        sesi.setNip(nip);
                                        sesi.setNama(dataLogin.getNama());
                                        sesi.setNama_Kantor(dataLogin.getNama_Kantor());
                                        sesi.setKode_Kantor(dataLogin.getKode_Kantor());
                                        sesi.createLoginSession(response.body().getToken());

                                        getDataPenerbangan();
                                        progressDialog.dismiss();
                                        activeonMoveActivity();
                                    } else {
                                        AppsHelper.ShowMessageError(pesan, c);
                                        progressDialog.dismiss();
                                    }
                                } else {
                                    pesan = "Periksa kembali nip dan Password";
                                    AppsHelper.ShowMessageError(pesan, c);
                                    progressDialog.dismiss();
                                }
                            }

                            private void activeonMoveActivity() {
                                Intent intent = new Intent(ActivityLogin.this, MenuActivity.class);
                                intent.putExtra("pesan", pesan);
                                intent.putExtra("status", result);

                                startActivity(intent);
                            }

                            @Override
                            public void onFailure(Call<ResponseLogin> call, Throwable t) {
                                pesan = "Gagal Terhubung ke Server";
                                progressDialog.dismiss();
                                AppsHelper.ShowMessageError(pesan, c);
                            }
                        });
                    }
                    catch (Exception ex)
                    {
                        checkInternet();
                        AppsHelper.ShowMessageInfo("Gagal Login", this);
                    }
                }
            }
            else
            {
                checkInternet();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.button_login)
    public void onViewClicked() {
            actionLogin();
    }

    private void getDataPenerbangan()
    {
        String content = "application/x-www-form-urlencoded";
        String accept = "application/json";

        NetworkClient.service.getDataPenerbangan("Bearer "+sesi.getToken(), content, accept,sesi.getKode_Kantor()).enqueue(new Callback<ResponsePenerbangan>() {
            @Override
            public void onResponse(Call<ResponsePenerbangan> call, Response<ResponsePenerbangan> response) {
                ArrayList<DataPenerbanganList> arrList = new ArrayList<>();
                int count = response.body().getDatas().size();
                ArrayList<DatasItem> datasItem = response.body().getDatas();

                for(int i = 0; i < count; i++)
                {
                    DataPenerbanganList dpl = new DataPenerbanganList();
                    dpl.setKODE_PENERBANGAN(datasItem.get(i).getKodePenerbangan());
                    dpl.setNAMA_PENERBANGAN(datasItem.get(i).getNamaPenerbangan());

                    arrList.add(dpl);
                }
                insertPenerbangan(arrList);
            }

            @Override
            public void onFailure(Call<ResponsePenerbangan> call, Throwable t) {

            }
        });
    }

    private void insertPenerbangan(ArrayList<DataPenerbanganList> arrayList)
    {
        try
        {
            for (int i = 0 ; i < arrayList.size(); i++) {
                String kode_penerbangan = arrayList.get(i).getKODE_PENERBANGAN();
                String nama_penerbangan = arrayList.get(i).getNAMA_PENERBANGAN();
                databaseHandler.insertPenerbangan(
                        kode_penerbangan,
                        nama_penerbangan
                );
            }
        }
        catch (Exception ex)
        {

        }
    }

    public boolean checkInternet(){
        boolean connectStatus = true;
        ConnectivityManager ConnectionManager=(ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=ConnectionManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()==true ) {
            connectStatus = true;
        }
        else {
            connectStatus = false;
            AppsHelper.ShowMessageInfo("No Internet Access", getApplicationContext());
        }
        return connectStatus;
    }
}
