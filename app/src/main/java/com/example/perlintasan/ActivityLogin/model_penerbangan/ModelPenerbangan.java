package com.example.perlintasan.ActivityLogin.model_penerbangan;

public class ModelPenerbangan {
    private String Kode_Penerbangan;
    private String Nama_Penerbangan;

    public ModelPenerbangan(String kode_Penerbangan, String nama_Penerbangan) {
        this.Kode_Penerbangan = kode_Penerbangan;
        this.Nama_Penerbangan = nama_Penerbangan;
    }

    public String getKode_Penerbangan() {
        return Kode_Penerbangan;
    }

    public void setKode_Penerbangan(String kode_Penerbangan) {
        Kode_Penerbangan = kode_Penerbangan;
    }

    public String getNama_Penerbangan() {
        return Nama_Penerbangan;
    }

    public void setNama_Penerbangan(String nama_Penerbangan) {
        Nama_Penerbangan = nama_Penerbangan;
    }
}
