package com.example.perlintasan.ActivityMenu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.perlintasan.ActivityHistory.ActivityHistory;
import com.example.perlintasan.ActivityLihatData.ActivityLihatData;
import com.example.perlintasan.ActivityLihatData.model.JamaahList;
import com.example.perlintasan.ActivityLogin.ActivityLogin;
import com.example.perlintasan.ActivityMenu.model.ResponseInsertMysql;
import com.example.perlintasan.ActivityTambahData.ActivityOpsiTambahData;
import com.example.perlintasan.DatabaseHelper.DatabaseHandler;
import com.example.perlintasan.R;
import com.example.perlintasan.helper.AppsHelper;
import com.example.perlintasan.helper.BaseActivity;
import com.example.perlintasan.network.NetworkClient;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuActivity extends BaseActivity {

    @BindView(R.id.textView2)
    TextView textView2;
    @BindView(R.id.txtNama)
    TextView txtNama;
    @BindView(R.id.txtKanim)
    TextView txtKanim;
    @BindView(R.id.btnTambahData)
    ImageView btnTambahData;
    @BindView(R.id.cvTambahData)
    CardView cvTambahData;
    @BindView(R.id.btnLihatData)
    ImageView btnLihatData;
    @BindView(R.id.cvLihatData)
    CardView cvLihatData;
    @BindView(R.id.btnReport)
    ImageView btnReport;
    @BindView(R.id.cvReport)
    CardView cvReport;
    @BindView(R.id.btnSync)
    ImageView btnSync;
    @BindView(R.id.cvSync)
    CardView cvSync;
    @BindView(R.id.ll)
    LinearLayout ll;

    Intent intent;
    String ket;
    public static DatabaseHandler databaseHandler;
    @BindView(R.id.txtKeberangkatan)
    TextView txtKeberangkatan;
    @BindView(R.id.txtKedatangan)
    TextView txtKedatangan;
    private ProgressDialog progressDialog;
    private String pesan = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);

        databaseHandler = new DatabaseHandler(this);

        Cursor cursorA = databaseHandler.getDataHistoryByNipAndKeterangan(sesi.getNip(), "Keberangkatan");
        Cursor cursorB = databaseHandler.getDataHistoryByNipAndKeterangan(sesi.getNip(), "Kedatangan");
        int a = cursorA.getCount();
        int b = cursorB.getCount();

        String keberangkatan = Integer.toString(a);
        String kedatangan = Integer.toString(b);

        txtNama.setText(sesi.getNama());
        txtKanim.setText(sesi.getNama_Kantor());
        txtKeberangkatan.setText(keberangkatan);
        txtKedatangan.setText(kedatangan);
    }

    @OnClick({R.id.btnTambahData, R.id.cvTambahData, R.id.btnLihatData, R.id.cvLihatData, R.id.btnReport, R.id.cvReport, R.id.btnSync, R.id.cvSync})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnTambahData:
                intent = new Intent(this, ActivityOpsiTambahData.class);
                startActivity(intent);
                break;
            case R.id.cvTambahData:
                intent = new Intent(this, ActivityOpsiTambahData.class);
                startActivity(intent);
                break;
            case R.id.btnLihatData:
                AlertDialog.Builder dialog = new AlertDialog.Builder(MenuActivity.this);
                CharSequence[] items = {"Keberangkatan", "Kedatangan"};
                dialog.setTitle("Pilih tindakan");
                dialog.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        if (i == 0) {
                            ket = "Keberangkatan";
                            toActivity(ket);
                        }
                        if (i == 1) {
                            ket = "Kedatangan";
                            toActivity(ket);
                        }
                    }
                });
                dialog.show();
                break;
            case R.id.cvLihatData:
                AlertDialog.Builder dialogs = new AlertDialog.Builder(MenuActivity.this);
                CharSequence[] item = {"Keberangkatan", "Kedatangan"};
                dialogs.setTitle("Pilih tindakan");
                dialogs.setItems(item, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        if (i == 0) {
                            ket = "Keberangkatan";
                            toActivity(ket);
                        }
                        if (i == 1) {
                            ket = "Kedatangan";
                            toActivity(ket);
                        }
                    }
                });
                dialogs.show();
                break;
            case R.id.btnReport:
                AlertDialog.Builder dialogsHis = new AlertDialog.Builder(this);
                CharSequence[] itmsHistory = {"Keberangkatan", "Kedatangan"};
                dialogsHis.setTitle("Pilih Keterangan");
                dialogsHis.setItems(itmsHistory, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        if (i == 0) {
                            Intent inte = new Intent(MenuActivity.this, ActivityHistory.class);
                            inte.putExtra("keterangan", "Keberangkatan");
                            startActivity(inte);
                        }
                        if (i == 1) {
                            Intent inte = new Intent(MenuActivity.this, ActivityHistory.class);
                            inte.putExtra("keterangan", "Kedatangan");
                            startActivity(inte);
                        }
                    }
                });
                dialogsHis.show();
                break;
            case R.id.cvReport:
                AlertDialog.Builder dialogsHistory = new AlertDialog.Builder(this);
                CharSequence[] itmsHist = {"Keberangkatan", "Kedatangan"};
                dialogsHistory.setTitle("Pilih Keterangan");
                dialogsHistory.setItems(itmsHist, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        if (i == 0) {
                            Intent inte = new Intent(MenuActivity.this, ActivityHistory.class);
                            inte.putExtra("keterangan", "Keberangkatan");
                            startActivity(inte);
                        }
                        if (i == 1) {
                            Intent inte = new Intent(MenuActivity.this, ActivityHistory.class);
                            inte.putExtra("keterangan", "Kedatangan");
                            startActivity(inte);
                        }
                    }
                });
                dialogsHistory.show();
                break;
            case R.id.btnSync:
                boolean inet_status = checkInternet();
                if (inet_status) {
                    AlertDialog.Builder dial = new AlertDialog.Builder(MenuActivity.this);
                    CharSequence[] pilihan = {"Keberangkatan", "Kedatangan"};
                    dial.setTitle("Pilih tindakan");
                    dial.setItems(pilihan, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            if (i == 0) {
                                ket = "Keberangkatan";
                                sync(ket);
                            }
                            if (i == 1) {
                                ket = "Kedatangan";
                                sync(ket);
                            }
                        }
                    });
                    dial.show();
                }
                break;
            case R.id.cvSync:
                inet_status = checkInternet();
                if (inet_status) {
                    AlertDialog.Builder dlgs = new AlertDialog.Builder(MenuActivity.this);
                    CharSequence[] itms = {"Keberangkatan", "Kedatangan"};
                    dlgs.setTitle("Pilih tindakan");
                    dlgs.setItems(itms, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            if (i == 0) {
                                ket = "Keberangkatan";
                                sync(ket);
                            }
                            if (i == 1) {
                                ket = "Kedatangan";
                                sync(ket);
                            }
                        }
                    });
                    dlgs.show();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.logout) {
            startActivity(new Intent(this, ActivityLogin.class));
            sesi.logoutUser();
            databaseHandler.deletePenerbangan();
            finish();
        } else if (item.getItemId() == R.id.profil) {
            /*startActivity(new Intent(this, GantiPasswordActivity.class));*/

        }

        return true;
    }

    private void toActivity(String ket) {
        intent = new Intent(this, ActivityLihatData.class);
        intent.putExtra("keterangan", ket);
        startActivity(intent);
    }

    private void sync(String ket) {
        sendData(sesi.getNip(), ket);
    }

    private void sendData(String nip, String ket) {
        Cursor cursor = databaseHandler.getDataByNipAndKeterangan(nip, ket);

        ArrayList<JamaahList> arrList = new ArrayList<>();
        int a = cursor.getCount();
        for (int i = 0; i < a; i++) {
            //while(cursor.moveToPosition()){
            //arrList.add(jl);
            JamaahList jl = new JamaahList();
            cursor.moveToPosition(i);
            jl.setID(cursor.getInt(0));
            jl.setNO_PASPOR(cursor.getString(1));
            jl.setNAMA(cursor.getString(2));
            jl.setTANGGAL_LAHIR(cursor.getString(3));
            jl.setTEMPAT_LAHIR(cursor.getString(4));
            jl.setWARGA_NEGARA(cursor.getString(5));
            jl.setJENIS_KELAMIN(cursor.getString(6));
            jl.setTANGGAL_PENGELUARAN(cursor.getString(7));
            jl.setTANGGAL_BERLAKU(cursor.getString(8));
            jl.setTANGGAL_MELINTAS(cursor.getString(9));
            jl.setKODE_TPI(cursor.getString(10));
            jl.setKODE_PENERBANGAN(cursor.getString(11));
            jl.setNIP(cursor.getString(12));
            jl.setIMAGE(cursor.getString(13));
            jl.setKETERANGAN(cursor.getString(14));
            jl.setCREATED_AT(cursor.getString(15));

            arrList.add(jl);
        }
        if (arrList.size() == 0) {
            AppsHelper.ShowMessageInfo("Data kosong", c);
        } else {
            /*Toast.makeText(MainActivity.this,"Data ada "+kelamin, Toast.LENGTH_SHORT).show();*/
            insertMYSQL(arrList);
        }
    }

    private void insertMYSQL(final ArrayList<JamaahList> arrayList) {
        progressDialog = new ProgressDialog(MenuActivity.this);
        progressDialog.setMax(arrayList.size());
        progressDialog.setMessage("BERHASIL");
        progressDialog.setTitle("Please Wait!");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setIndeterminate(true);
        progressDialog.setProgress(0);
        progressDialog.show();

        int count = arrayList.size();
        for (int i = 0; i < arrayList.size(); i++) {

            String no_paspor = arrayList.get(i).getNO_PASPOR();
            String nama = arrayList.get(i).getNAMA();
            String tanggal_lahir = arrayList.get(i).getTANGGAL_LAHIR();
            String tempat_lahir = arrayList.get(i).getTEMPAT_LAHIR();
            String warga_negara = arrayList.get(i).getWARGA_NEGARA();
            String jenis_kelamin = arrayList.get(i).getJENIS_KELAMIN();
            String tanggal_pengeluaran = arrayList.get(i).getTANGGAL_PENGELUARAN();
            String tanggal_berlaku = arrayList.get(i).getTANGGAL_BERLAKU();
            String tanggal_melintas = arrayList.get(i).getTANGGAL_MELINTAS();
            String kode_tpi = arrayList.get(i).getKODE_TPI();
            String kode_penerbangan = arrayList.get(i).getKODE_PENERBANGAN();
            String nip = arrayList.get(i).getNIP();
            String image = arrayList.get(i).getIMAGE();
            String keterangan = arrayList.get(i).getKETERANGAN();
            String created_at = arrayList.get(i).getCREATED_AT();

            String token = sesi.getToken();
            String content = "application/x-www-form-urlencoded";
            String accept = "application/json";

            for (int a = 1; a == arrayList.size(); a++) {
                progressDialog.setProgress(a);
            }

            NetworkClient.service.Sync("Bearer " + token, content, accept, no_paspor, nama, tanggal_lahir, tempat_lahir, warga_negara, jenis_kelamin, tanggal_pengeluaran, tanggal_berlaku, tanggal_melintas, kode_tpi, kode_penerbangan
                    , nip, image, keterangan, created_at).enqueue(new Callback<ResponseInsertMysql>() {
                public void onResponse(Call<ResponseInsertMysql> call, Response<ResponseInsertMysql> response) {
                    if (response.isSuccessful()) {
                        Boolean status = response.body().isStatus();
                        String pesan = response.body().getPesan();

                        progressDialog.dismiss();
                        insertHistory(arrayList);
                        AppsHelper.ShowMessageSuccess(pesan, c);
                    }
                }

                public void onFailure(Call<ResponseInsertMysql> call, Throwable t) {
                    progressDialog.dismiss();
                    AppsHelper.ShowMessageError("Gagal koneksi dengan server", c);
                }
            });
        }
    }

    private void insertHistory(final ArrayList<JamaahList> arrayList) {
        try {
            for (int i = 0; i < arrayList.size(); i++) {
                int id = arrayList.get(i).getID();
                String no_paspor = arrayList.get(i).getNO_PASPOR();
                String nama = arrayList.get(i).getNAMA();
                String tanggal_lahir = arrayList.get(i).getTANGGAL_LAHIR();
                String tempat_lahir = arrayList.get(i).getTEMPAT_LAHIR();
                String warga_negara = arrayList.get(i).getWARGA_NEGARA();
                String jenis_kelamin = arrayList.get(i).getJENIS_KELAMIN();
                String tanggal_pengeluaran = arrayList.get(i).getTANGGAL_PENGELUARAN();
                String tanggal_berlaku = arrayList.get(i).getTANGGAL_BERLAKU();
                String tanggal_melintas = arrayList.get(i).getTANGGAL_MELINTAS();
                String kode_tpi = arrayList.get(i).getKODE_TPI();
                String kode_penerbangan = arrayList.get(i).getKODE_PENERBANGAN();
                String nip = arrayList.get(i).getNIP();
                String image = arrayList.get(i).getIMAGE();
                String keterangan = arrayList.get(i).getKETERANGAN();
                String created_at = arrayList.get(i).getCREATED_AT();

                databaseHandler.insertHistory(
                        no_paspor,
                        nama,
                        tanggal_lahir,
                        tempat_lahir,
                        warga_negara,
                        jenis_kelamin,
                        tanggal_pengeluaran,
                        tanggal_berlaku,
                        tanggal_melintas,
                        kode_tpi,
                        kode_penerbangan,
                        nip,
                        image,
                        keterangan,
                        created_at
                );
                databaseHandler.delete(id);
            }
        } catch (Exception ex) {
        }
    }

    public boolean checkInternet() {
        boolean connectStatus = true;
        ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
            connectStatus = true;
            AppsHelper.ShowMessageSuccess("Internet Access", getApplicationContext());
        } else {
            connectStatus = false;
            AppsHelper.ShowMessageInfo("No Internet Access", getApplicationContext());
        }
        return connectStatus;
    }
}
