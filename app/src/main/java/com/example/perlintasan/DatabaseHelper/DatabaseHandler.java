package com.example.perlintasan.DatabaseHelper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 2;

    static final String DATABASE_NAME = "Perlintasan.db";

    public static final String TABLE_NAME = "ms_jamaah";
    public static final String TABLE_NAME_HISTORY = "ms_history";
    public static final String TABLE_PENERBANGAN = "ms_penerbangan";

    public static final String ID = "id";
    public static final String NO_PASPOR = "no_paspor";
    public static final String NAMA = "nama";
    public static final String TANGGAL_LAHIR = "tanggal_lahir";
    public static final String TEMPAT_LAHIR = "tempat_lahir";
    public static final String WARGA_NEGARA = "warga_negara";
    public static final String JENIS_KELAMIN = "jenis_kelamin";
    public static final String TANGGAL_PENGELUARAN = "tanggal_pengeluaran";
    public static final String TANGGAL_BERLAKU = "tanggal_berlaku";
    public static final String TANGGAL_MELINTAS = "tanggal_melintas";
    public static final String KODE_TPI = "kode_tpi";
    public static final String KODE_PENERBANGAN = "kode_penerbangan";
    public static final String NIP = "nip";
    public static final String IMAGE = "image";
    public static final String KETERANGAN = "keterangan";
    public static final String CREATED_AT = "created_at";

    public static final String KODEPENERBANGAN = "kode_penerbangan";
    public static final String NAMAPENERBANGAN = "nama_penerbangan";

    final String SQL_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
            ID + " INTEGER PRIMARY KEY autoincrement, " +
            NO_PASPOR + " TEXT NOT NULL, " +
            NAMA + " TEXT NOT NULL, " +
            TANGGAL_LAHIR + " DATE NOT NULL, " +
            TEMPAT_LAHIR + " TEXT NOT NULL, " +
            WARGA_NEGARA + " TEXT NOT NULL, " +
            JENIS_KELAMIN + " TEXT NOT NULL, " +
            TANGGAL_PENGELUARAN + " TEXT NOT NULL, " +
            TANGGAL_BERLAKU + " TEXT NOT NULL, " +
            TANGGAL_MELINTAS + " TEXT NOT NULL, " +
            KODE_TPI + " TEXT NOT NULL, " +
            KODE_PENERBANGAN + " TEXT NOT NULL, " +
            NIP + " TEXT NOT NULL, " +
            IMAGE + " TEXT NOT NULL, " +
            KETERANGAN + " TEXT NOT NULL, " +
            CREATED_AT + " TEXT NOT NULL " +
            " )";

    final String SQL_CREATE_TABLE_HISTORY = "CREATE TABLE " + TABLE_NAME_HISTORY + " (" +
            ID + " INTEGER PRIMARY KEY autoincrement, " +
            NO_PASPOR + " TEXT NOT NULL, " +
            NAMA + " TEXT NOT NULL, " +
            TANGGAL_LAHIR + " DATE NOT NULL, " +
            TEMPAT_LAHIR + " TEXT NOT NULL, " +
            WARGA_NEGARA + " TEXT NOT NULL, " +
            JENIS_KELAMIN + " TEXT NOT NULL, " +
            TANGGAL_PENGELUARAN + " TEXT NOT NULL, " +
            TANGGAL_BERLAKU + " TEXT NOT NULL, " +
            TANGGAL_MELINTAS + " TEXT NOT NULL, " +
            KODE_TPI + " TEXT NOT NULL, " +
            KODE_PENERBANGAN + " TEXT NOT NULL, " +
            NIP + " TEXT NOT NULL, " +
            IMAGE + " TEXT NOT NULL, " +
            KETERANGAN + " TEXT NOT NULL, " +
            CREATED_AT + " TEXT NOT NULL " +
            " )";

    final String SQL_CREATE_TABLE_PENERBANGAN = "CREATE TABLE " + TABLE_PENERBANGAN + " (" +
            KODEPENERBANGAN + " TEXT NOT NULL, " +
            NAMAPENERBANGAN + " TEXT NOT NULL " +
            " )";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
        db.execSQL(SQL_CREATE_TABLE_HISTORY);
        db.execSQL(SQL_CREATE_TABLE_PENERBANGAN);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_HISTORY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PENERBANGAN);
        onCreate(db);
    }

    public ArrayList<HashMap<String, String>> getAllDataByNipAndKeterangan(String nip, String keterangan) {
        ArrayList<HashMap<String, String>> wordList;
        wordList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + NIP + "=" +nip+ " AND " + KETERANGAN + "=" +keterangan;
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(ID, cursor.getString(0));
                map.put(NO_PASPOR, cursor.getString(1));
                map.put(NAMA, cursor.getString(2));

                map.put(TANGGAL_LAHIR, cursor.getString(3));
                map.put(TEMPAT_LAHIR, cursor.getString(4));
                map.put(WARGA_NEGARA, cursor.getString(5));

                map.put(JENIS_KELAMIN, cursor.getString(6));
                map.put(TANGGAL_PENGELUARAN, cursor.getString(7));
                map.put(TANGGAL_BERLAKU, cursor.getString(8));

                map.put(TANGGAL_MELINTAS, cursor.getString(9));
                map.put(KODE_TPI, cursor.getString(10));
                map.put(KODE_PENERBANGAN, cursor.getString(11));

                map.put(NIP, cursor.getString(12));
                map.put(IMAGE, cursor.getString(13));
                map.put(KETERANGAN, cursor.getString(14));
                map.put(CREATED_AT, cursor.getString(15));
                wordList.add(map);
            } while (cursor.moveToNext());
        }

        Log.e("select sqlite ", "" + wordList);

        database.close();
        return wordList;
    }

    public void insert(String no_paspor, String nama, String tanggal_lahir, String tempat_lahir, String warga_negara, String jenis_kelamin, String tanggal_pengeluaran, String tanggal_berlaku,
                              String tanggal_melintas, String kode_tpi, String kode_penerbangan, String nip, String image, String keterangan, String created_at) {
        SQLiteDatabase database = this.getWritableDatabase();
        String queryValues = "INSERT INTO " + TABLE_NAME + " (no_paspor, nama, tanggal_lahir, tempat_lahir, warga_negara, jenis_kelamin, tanggal_pengeluaran, tanggal_berlaku," +
                "tanggal_melintas, kode_tpi, kode_penerbangan, nip, image, keterangan, created_at) " +
                "VALUES ('" + no_paspor + "', '" + nama + "', '" + tanggal_lahir + "', '" + tempat_lahir + "', '" + warga_negara + "', '" + jenis_kelamin + "', '" + tanggal_pengeluaran + "', '" + tanggal_berlaku + "'" +
                ", '" + tanggal_melintas + "', '" + kode_tpi + "', '" + kode_penerbangan + "', '" + nip + "', '" + image + "', '" + keterangan + "', '" + created_at + "')";

        Log.e("insert sqlite ", "" + queryValues);
        database.execSQL(queryValues);
        database.close();
    }

    public void insertHistory(String no_paspor, String nama, String tanggal_lahir, String tempat_lahir, String warga_negara, String jenis_kelamin, String tanggal_pengeluaran, String tanggal_berlaku,
                       String tanggal_melintas, String kode_tpi, String kode_penerbangan, String nip, String image, String keterangan, String created_at) {
        SQLiteDatabase database = this.getWritableDatabase();
        String queryValues = "INSERT INTO " + TABLE_NAME_HISTORY + " (no_paspor, nama, tanggal_lahir, tempat_lahir, warga_negara, jenis_kelamin, tanggal_pengeluaran, tanggal_berlaku," +
                "tanggal_melintas, kode_tpi, kode_penerbangan, nip, image, keterangan, created_at) " +
                "VALUES ('" + no_paspor + "', '" + nama + "', '" + tanggal_lahir + "', '" + tempat_lahir + "', '" + warga_negara + "', '" + jenis_kelamin + "', '" + tanggal_pengeluaran + "', '" + tanggal_berlaku + "'" +
                ", '" + tanggal_melintas + "', '" + kode_tpi + "', '" + kode_penerbangan + "', '" + nip + "', '" + image + "', '" + keterangan + "', '" + created_at + "')";

        Log.e("insert sqlite ", "" + queryValues);
        database.execSQL(queryValues);
        database.close();
    }

    public void insertPenerbangan(String kode_penerbangan, String nama_penerbangan) {
        SQLiteDatabase database = this.getWritableDatabase();
        String queryValues = "INSERT INTO " + TABLE_PENERBANGAN + " (kode_penerbangan, nama_penerbangan) " +
                "VALUES ('" + kode_penerbangan + "', '" + nama_penerbangan + "')";

        Log.e("insert sqlite ", "" + queryValues);
        database.execSQL(queryValues);
        database.close();
    }

    public void update(int id, String no_paspor, String nama, String tanggal_lahir, String tempat_lahir, String warga_negara, String jenis_kelamin, String tanggal_pengeluaran, String tanggal_berlaku,
                       String tanggal_melintas, String kode_tpi, String nip, String image ) {
        SQLiteDatabase database = this.getWritableDatabase();

        String updateQuery = "UPDATE " + TABLE_NAME + " SET "
                + NO_PASPOR + "='" + no_paspor + "', "
                + NAMA + "='" + nama + "', "
                + TANGGAL_LAHIR + "='" + tanggal_lahir + "', "
                + TEMPAT_LAHIR + "='" + tempat_lahir + "', "
                + WARGA_NEGARA + "='" + warga_negara + "', "
                + JENIS_KELAMIN + "='" + jenis_kelamin + "', "
                + TANGGAL_PENGELUARAN + "='" + tanggal_pengeluaran + "', "
                + TANGGAL_BERLAKU + "='" + tanggal_berlaku + "', "
                + TANGGAL_MELINTAS + "='" + tanggal_melintas + "', "
                + KODE_TPI + "='" + kode_tpi + "', "
                + NIP + "='" + nip + "', "
                + IMAGE + "='" + image + "' "
                + "WHERE " + ID + "=" +  id ;
        database.execSQL(updateQuery);
        database.close();
    }

    public void delete(int id) {
        SQLiteDatabase database = this.getWritableDatabase();

        String updateQuery = "DELETE FROM " + TABLE_NAME + " WHERE " + ID + "=" + "'" + id + "'";
        Log.e("update sqlite ", updateQuery);
        database.execSQL(updateQuery);
        database.close();
    }

    public Cursor cekPaspor(String no_paspor)
    {
        String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + NO_PASPOR + " = " + "'" + no_paspor + "'";
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(selectQuery, null);
    }

    public Cursor getDataByNipAndKeterangan(String nip, String keterangan)
    {
        String selectQuery = "SELECT * FROM ms_jamaah WHERE " +NIP+ " = '" +nip+ "' AND " +KETERANGAN + " = '" +keterangan+ "' ";
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(selectQuery, null);
    }

    public Cursor getDataHistoryByNipAndKeterangan(String nip, String keterangan)
    {
        String selectQuery = "SELECT * FROM ms_history WHERE " +NIP+ " = '" +nip+ "' AND " +KETERANGAN + " = '" +keterangan+ "' ";
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(selectQuery, null);
    }

    public Cursor getDataHistoryByNipNamaAndKeterangan(String nip, String keterangan, String nama)
    {
        String selectQuery = "SELECT * FROM ms_history WHERE nama LIKE '" +nama+ "' AND " +NIP+ " = '" +nip+ "' AND " +KETERANGAN + " = '" +keterangan+ "' ";
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(selectQuery, null);
    }

    public Cursor getDataById(int id)
    {
        String selectQuery = "SELECT * FROM ms_jamaah Where " + ID + " = " + id;
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(selectQuery, null);
    }

    public Cursor getAllData()
    {
        String selectQuery = "SELECT * FROM ms_jamaah";
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(selectQuery, null);
    }

    public Cursor getAllDataHostory()
    {
        String selectQuery = "SELECT * FROM ms_history";
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(selectQuery, null);
    }

    public Cursor getDataPenerbangan()
    {
        String selectQuery = "SELECT kode_penerbangan FROM ms_penerbangan";
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(selectQuery, null);
    }

    public void deletePenerbangan() {
        SQLiteDatabase database = this.getWritableDatabase();

        String updateQuery = "DELETE FROM " + TABLE_PENERBANGAN;
        Log.e("update sqlite ", updateQuery);
        database.execSQL(updateQuery);
        database.close();
    }

    public List<String> getKodePenerbangan()
    {
        List<String> userlist=new ArrayList<>();
        //get readable database
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery("SELECT kode_penerbangan FROM ms_penerbangan",null);
        if(cursor.moveToFirst())
        {
            do {
                userlist.add(cursor.getString(0));
            }while (cursor.moveToNext());
        }
        //close the cursor
        cursor.close();
        //close the database
        db.close();
        return userlist;
    }

}
