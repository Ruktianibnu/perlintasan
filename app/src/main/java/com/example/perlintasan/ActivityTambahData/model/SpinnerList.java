package com.example.perlintasan.ActivityTambahData.model;

public class SpinnerList {
    String KODE_PENERBANGAN;

    public SpinnerList(String KODE_PENERBANGAN) {
        this.KODE_PENERBANGAN = KODE_PENERBANGAN;
    }

    public String getKODE_PENERBANGAN() {
        return KODE_PENERBANGAN;
    }

    public void setKODE_PENERBANGAN(String KODE_PENERBANGAN) {
        this.KODE_PENERBANGAN = KODE_PENERBANGAN;
    }
}
