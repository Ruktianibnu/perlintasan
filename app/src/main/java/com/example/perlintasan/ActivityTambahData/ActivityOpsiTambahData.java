package com.example.perlintasan.ActivityTambahData;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.example.perlintasan.ActivityTambahData.model.ModelSpinner;
import com.example.perlintasan.DatabaseHelper.DatabaseHandler;
import com.example.perlintasan.R;
import com.example.perlintasan.helper.AppsHelper;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityOpsiTambahData extends AppCompatActivity {

    @BindView(R.id.spOption)
    MaterialBetterSpinner spOption;
    @BindView(R.id.btnLanjut)
    Button btnLanjut;

    public static DatabaseHandler databaseHandler;
    ArrayList<ModelSpinner> mList;
    @BindView(R.id.spKodePenerbangan)
    MaterialBetterSpinner spKodePenerbangan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opsi_tambah_data);
        ButterKnife.bind(this);

        String[] items = new String[]{"Keberangkatan", "Kedatangan"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        spOption.setAdapter(adapter);

        databaseHandler = new DatabaseHandler(this);

        loadSpinner();
    }

    @OnClick(R.id.btnLanjut)
    public void onViewClicked() {
        String kode_penerbangan = spKodePenerbangan.getText().toString();
        String ket = spOption.getText().toString();
        if (ket.equals("")) {
            AppsHelper.ShowMessageWarning("pilih keberangkatan atau kedatangan", this);
        } else if (kode_penerbangan.isEmpty()) {
            AppsHelper.ShowMessageWarning("input nomor penerbangan", this);
        } else {
            Intent myIntent = new Intent(ActivityOpsiTambahData.this, ActivityTambahData.class);
            myIntent.putExtra("keterangan", ket);
            myIntent.putExtra("kode_penerbangan", kode_penerbangan);
            startActivity(myIntent);
        }
    }

    private void loadSpinner() {
        // database handler
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());

        // Spinner Drop down elements
        List<String> lables = db.getKodePenerbangan();

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spKodePenerbangan.setAdapter(dataAdapter);
    }

}
