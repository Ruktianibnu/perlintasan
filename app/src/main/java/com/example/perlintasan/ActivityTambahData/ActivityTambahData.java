package com.example.perlintasan.ActivityTambahData;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.perlintasan.ActivityLihatData.ActivityLihatData;
import com.example.perlintasan.DatabaseHelper.DatabaseHandler;
import com.example.perlintasan.R;
import com.example.perlintasan.helper.AppsHelper;
import com.example.perlintasan.helper.BaseActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityTambahData extends BaseActivity {

    @BindView(R.id.tvKeterangan)
    TextView tvKeterangan;
    @BindView(R.id.imgCam)
    ImageView imgCam;
    @BindView(R.id.btnFoto)
    AppCompatButton btnFoto;
    @BindView(R.id.textView3)
    TextView textView3;
    @BindView(R.id.etNoPaspor)
    EditText etNoPaspor;
    @BindView(R.id.etNama)
    EditText etNama;
    @BindView(R.id.textView4)
    TextView textView4;
    @BindView(R.id.rbP)
    RadioButton rbP;
    @BindView(R.id.rbL)
    RadioButton rbL;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.etTempatLahir)
    EditText etTempatLahir;
    @BindView(R.id.etTglLahir)
    EditText etTglLahir;
    @BindView(R.id.etWN)
    EditText etWN;
    @BindView(R.id.etKantor)
    EditText etKantor;
    @BindView(R.id.etTglPengeluaran)
    EditText etTglPengeluaran;
    @BindView(R.id.etTglBerlaku)
    EditText etTglBerlaku;
    @BindView(R.id.btnBatal)
    Button btnBatal;
    @BindView(R.id.btnSimpan)
    Button btnSimpan;
    @BindView(R.id.LinearLayout01)
    LinearLayout LinearLayout01;
    @BindView(R.id.RelativeLayout01)
    RelativeLayout RelativeLayout01;
    @BindView(R.id.ScrollView01)
    ScrollView ScrollView01;

    Calendar c;
    DatePickerDialog dpd;

    final int REQUEST_CODE_CAMERA = 100;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final int REQUEST_CAPTURE_IMAGE = 100;
    String imageFilePath = "";
    String no_paspor,  nama,  tanggal_lahir,  tempat_lahir,  warga_negara,  jenis_kelamin,  tanggal_pengeluaran,  tanggal_berlaku, tanggal_melintas,  kode_tpi,  kode_penerbangan,  nip,  image,  keterangan,  created_at, nama_tpi;

    String date;
    String imageInBase64Str = "";

    public static DatabaseHandler databaseHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_data);
        ButterKnife.bind(this);

        databaseHandler = new DatabaseHandler(this);

        Date dateNow = Calendar.getInstance().getTime();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        date = df.format(dateNow);

        etTglLahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c = Calendar.getInstance();
                int day = c.get(Calendar.DAY_OF_MONTH);
                int monthOfYear = c.get(Calendar.MONTH);
                int year = c.get(Calendar.YEAR);

                dpd = new DatePickerDialog(ActivityTambahData.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int mDay) {
                        int month = monthOfYear + 1;
                        String formattedMonth = "" + month;
                        String formattedDayOfMonth = "" + mDay;

                        if(month < 10){

                            formattedMonth = "0" + month;
                        }
                        if(mDay < 10){

                            formattedDayOfMonth = "0" + mDay;
                        }

                        etTglLahir.setText(year + "-" + formattedMonth + "-" +formattedDayOfMonth);
                    }
                },  year, monthOfYear, day);
                dpd.show();
            }
        });

        etTglPengeluaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c = Calendar.getInstance();
                int day = c.get(Calendar.DAY_OF_MONTH);
                int monthOfYear = c.get(Calendar.MONTH);
                int year = c.get(Calendar.YEAR);

                dpd = new DatePickerDialog(ActivityTambahData.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int mDay) {
                        int month = monthOfYear + 1;
                        String formattedMonth = "" + month;
                        String formattedDayOfMonth = "" + mDay;

                        if(month < 10){

                            formattedMonth = "0" + month;
                        }
                        if(mDay < 10){

                            formattedDayOfMonth = "0" + mDay;
                        }

                        etTglPengeluaran.setText(year + "-" + formattedMonth + "-" +formattedDayOfMonth);
                    }
                },  year, monthOfYear, day);
                dpd.show();
            }
        });

        etTglBerlaku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c = Calendar.getInstance();
                int day = c.get(Calendar.DAY_OF_MONTH);
                int monthOfYear = c.get(Calendar.MONTH);
                int year = c.get(Calendar.YEAR);

                dpd = new DatePickerDialog(ActivityTambahData.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int mDay) {
                        int month = monthOfYear + 1;
                        String formattedMonth = "" + month;
                        String formattedDayOfMonth = "" + mDay;

                        if(month < 10){

                            formattedMonth = "0" + month;
                        }
                        if(mDay < 10){

                            formattedDayOfMonth = "0" + mDay;
                        }

                        etTglBerlaku.setText(year + "-" + formattedMonth + "-" +formattedDayOfMonth);
                    }
                },  year, monthOfYear, day);
                dpd.show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.btnFoto, R.id.btnBatal, R.id.btnSimpan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnFoto:
                if (ContextCompat.checkSelfPermission(this,Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this,Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                {
                    requestPermissions(new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_CAMERA_PERMISSION_CODE);
                }
                else
                {
                    openCameraIntent();
                }
                break;
            case R.id.btnBatal:
                    ClearData();
                break;
            case R.id.btnSimpan:
                insertData();
                break;
        }
    }

    private void insertData()
    {
        String kode_penerbangan = getIntent().getStringExtra("kode_penerbangan");
        keterangan = getIntent().getStringExtra("keterangan");
        String nip = sesi.getNip();
        String kode_tpi = sesi.getKode_Kantor();
        String nopaspor = etNoPaspor.getText().toString();

        Cursor cursor = databaseHandler.cekPaspor(nopaspor);
        cursor.moveToFirst();
        int count = cursor.getCount();
        if (count > 0) {
            AppsHelper.ShowMessageWarning("Nomor paspor "+nopaspor+" sudah terdaftar di "+keterangan, this);
        } else {
            try
            {
                no_paspor = etNoPaspor.getText().toString();
                nama = etNama.getText().toString();
                tanggal_lahir = etTglLahir.getText().toString();
                tempat_lahir = etTempatLahir.getText().toString();
                warga_negara = etWN.getText().toString();

                int selectedId = radioGroup.getCheckedRadioButtonId();
                RadioButton rb = (RadioButton) findViewById(selectedId);
                jenis_kelamin = rb.getText().toString();

                tanggal_pengeluaran = etTglPengeluaran.getText().toString();
                tanggal_berlaku = etTglBerlaku.getText().toString();
                tanggal_melintas = date;
                created_at = date;
                nama_tpi = etKantor.getText().toString();
                image = imageInBase64Str;
                sesi.setKeterangan(keterangan);

                if(image.isEmpty())
                {
                    etNoPaspor.setError("please Take a photo");
                    etNoPaspor.requestFocus();
                }
                else if(no_paspor.isEmpty())
                {
                    etNoPaspor.setError("please insert paspor");
                    etNoPaspor.requestFocus();
                }
                else if(nama.isEmpty())
                {
                    etNama.setError("please insert nama");
                    etNama.requestFocus();
                }
                else if(tanggal_lahir.isEmpty())
                {
                    etTglLahir.setError("please insert tanggal lahir");
                    etTglLahir.requestFocus();
                }
                else if(tempat_lahir.isEmpty())
                {
                    etTempatLahir.setError("please insert tmpat lahir");
                    etTempatLahir.requestFocus();
                }
                else if(warga_negara.isEmpty())
                {
                    etWN.setError("please insert warga negara");
                    etWN.requestFocus();
                }
                else if(tanggal_pengeluaran.isEmpty())
                {
                    etTglPengeluaran.setError("please insert tanggal pengeluaran");
                    etTglPengeluaran.requestFocus();
                }
                else if(tanggal_berlaku.isEmpty())
                {
                    etTglBerlaku.setError("please insert tanggal berlaku");
                    etTglBerlaku.requestFocus();
                }
                else if(nama_tpi.isEmpty())
                {
                    etKantor.setError("please insert kantor");
                    etKantor.requestFocus();
                }
                else {
                    databaseHandler.insert(
                            no_paspor,
                            nama,
                            tanggal_lahir,
                            tempat_lahir,
                            warga_negara,
                            jenis_kelamin,
                            tanggal_pengeluaran,
                            tanggal_berlaku,
                            tanggal_melintas,
                            nama_tpi,
                            kode_penerbangan,
                            nip,
                            image,
                            keterangan,
                            created_at
                    );
                    AppsHelper.ShowMessageSuccess("Add Successfully", this);
                    ClearData();
                }
            } catch (Exception ex) {
                AppsHelper.ShowMessageError("Gagal Tambah Data", this);
                ex.printStackTrace();

            }
        }
    }

    private void ClearData()
    {
        etNoPaspor.setText("");
        etNama.setText("");
        etTglLahir.setText("");
        etTempatLahir.setText("");
        etWN.setText("");
        etTglPengeluaran.setText("");
        etTglBerlaku.setText("");
        etKantor.setText("");
        imgCam.setImageDrawable(getDrawable(R.drawable.photo_camera));
        scaleImage(imgCam, 200);

        etNoPaspor.requestFocus();
    }

    private void openCameraIntent() {

        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE
        );
        if(pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image
            File photoFile = null;
            try {
                photoFile = createImageFile();
            }
            catch(Exception ex) {
                Log.e("ActivityTambahData", "openCameraIntent: "+ex.toString());
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,"com.example.perlintasan.provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        REQUEST_CAPTURE_IMAGE);
            }
        }
    }

    private File createImageFile() {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,   //prefix
                    ".jpg",          //suffix
                    storageDir       //directory
            );
        } catch (IOException e) {
            //Log.e("ActivityTambahData", "createImageFile: "+e.toString());
        }
        imageFilePath = image.getAbsolutePath();
        return image;
    }

    private void scaleImage(ImageView view, int boundBoxInDp)
    {
        // Get the ImageView and its bitmap
        Drawable drawing = view.getDrawable();
        Bitmap bitmap = ((BitmapDrawable)drawing).getBitmap();

        // Get current dimensions
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        // Determine how much to scale: the dimension requiring less scaling is
        // closer to the its side. This way the image always stays inside your
        // bounding box AND either x/y axis touches it.
        float xScale = ((float) boundBoxInDp) / width;
        float yScale = ((float) boundBoxInDp) / height;
        float scale = (xScale <= yScale) ? xScale : yScale;

        // Create a matrix for the scaling and add the scaling data
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        matrix.postRotate(90);

        // Create a new bitmap and convert it to a format understood by the ImageView
        Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        BitmapDrawable result = new BitmapDrawable(scaledBitmap);
        width = scaledBitmap.getWidth();
        height = scaledBitmap.getHeight();

        // Apply the scaled bitmap
        view.setImageDrawable(result);

        // Now change ImageView's dimensions to match the scaled image
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
        params.width = width;
        params.height = height;
        view.setLayoutParams(params);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE_CAMERA && resultCode == RESULT_OK){
            Bitmap bitmap = null;
            try {
                File file = new File(imageFilePath);
                bitmap = MediaStore.Images.Media
                        .getBitmap(ActivityTambahData.this.getContentResolver(), Uri.fromFile(file));

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (bitmap != null) {
                /*Matrix matrix = new Matrix();
                float angle = 90;
                matrix.postRotate(angle);*/
                imgCam.setImageBitmap(bitmap);
                ByteArrayOutputStream bao = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG,0,bao);
                byte[] imageInByte = bao.toByteArray();
                imageInBase64Str = Base64.encodeToString(imageInByte, Base64.DEFAULT);
                scaleImage(imgCam, 1000);
            }
        }
        super.onActivityReenter(resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.mybutton) {
            // do something here
            Intent myIntent = new Intent(ActivityTambahData.this, ActivityLihatData.class);
            myIntent.putExtra("keterangan", getIntent().getStringExtra("keterangan"));
            startActivity(myIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }
}
