package com.example.perlintasan.ActivityTambahData.model;

public class ModelSpinner {
    private String kode;

    public ModelSpinner(String kode_Penerbangan) {
        this.kode = kode_Penerbangan;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }
}
